#include <iostream>

class LongInt {
public:
    std::string digits;

    explicit LongInt(std::string initialDigits = ""):digits(initialDigits) {
    }

    LongInt add(LongInt other){
        LongInt res;
        // plain add
        // res.digits = ...

        return res;
    }

    void print() {
        for (int i = 0; i < digits.length(); ++i) {
            std::cout << digits[i];
        }
        std::cout << std::endl;
    }
};

int main() {
    LongInt a("123"), b("215") ,c;

    c = a.add(b);
    // c = a + b;

    c.print();

    return 0;
}